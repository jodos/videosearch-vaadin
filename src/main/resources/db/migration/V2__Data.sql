INSERT INTO category VALUES (1, 1,'Action/Aventure');
INSERT INTO category VALUES (2, 2,'Drame');
INSERT INTO category VALUES (3, 3,'Horreur');
INSERT INTO category VALUES (4, 4,'Comédie');
INSERT INTO category VALUES (5, 5,'Enfants');
INSERT INTO category VALUES (6, 6,'Familial');
INSERT INTO category VALUES (7, 7,'Science Fiction');

INSERT INTO movie VALUES (1, 4, 'Piège de Cristal','Bruce Willis dans un film d''action et d''aventure','R','CHF 20.95','CHF 2.50', 1);
INSERT INTO movie VALUES (2, 6, 'Dumb & Dumber','Film fou!','PG-13','CHF 15.95','CHF 3.00', 4);
INSERT INTO movie VALUES (5, 34, 'La liste de Schindler','Film très intense au sujet de l''Holocauste.','R','CHF 20.00','CHF 3.00', 2);
INSERT INTO movie VALUES (6, 23, 'The Mask','De grands effets spéciaux basés sue une BD.','PG-13','CHF 20.00','CHF 3.00', 4);
INSERT INTO movie VALUES (7, 5, 'Balto','Animation au sujet de chiens d''Alaska.','G','CHF 25.00','CHF 3.00', 4);
INSERT INTO movie VALUES (8, 8, 'Rapid Fire','Le meilleur film d''arts martiaux de Brandon Lee.','R','CHF 25.00','CHF 3.00', 1);
INSERT INTO movie VALUES (9, 9, 'Star Trek V','The entreprise voyage dans le temps.','PG','CHF 25.00','CHF 3.00', 7);
INSERT INTO movie VALUES (10, 54, 'Crimson Tide','Mésaventures d''un sous-marin.','R' ,'CHF 25.00','CHF 3.00', 2);
INSERT INTO movie VALUES (11, 23, 'Monty Python et le sacré Graal','Folie à la cour du roi Arthur.','PG', 'CHF 20.00','CHF 3.00', 4);
INSERT INTO movie VALUES (18, 13, 'Piège de Cristal 2','Bruce Willis dans un film d''action et d''aventure.','PG-13', 'CHF 0.00','CHF 0.00', 4);