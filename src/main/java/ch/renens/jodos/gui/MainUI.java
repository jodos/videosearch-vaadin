package ch.renens.jodos.gui;

import ch.renens.jodos.gui.bottom.ResultPanel;
import ch.renens.jodos.model.Movie;
import ch.renens.jodos.service.MovieService;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Theme("valo")
@SpringUI
public class MainUI extends UI {

    private static final Logger logger = LoggerFactory.getLogger(MainUI.class);

    @Autowired
    private MovieService movieService;

    @Override
    protected void init(VaadinRequest request) {

        Panel p1 = new Panel();
        Panel p2 = new Panel();

        p1.setSizeFull();
        p2.setSizeFull();

        List<Movie> movies = movieService.findAll();

        GridLayout gridLayout = new GridLayout(1, 3);
        gridLayout.setSizeFull();

        gridLayout.setMargin(true);
        gridLayout.setSpacing(true);
        gridLayout.setResponsive(true);

        ResultPanel resultPanel = new ResultPanel(movies);

        gridLayout.addComponent(p1);
        gridLayout.addComponent(p2);
        gridLayout.addComponent(resultPanel);

        setContent(gridLayout);

    }
}
