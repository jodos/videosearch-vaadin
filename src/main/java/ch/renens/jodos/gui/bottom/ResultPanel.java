package ch.renens.jodos.gui.bottom;

import ch.renens.jodos.model.Movie;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.List;

public class ResultPanel extends Panel {

    private final Label resultPanelLabel;
    private final Grid grid;
    private final TextField textField;

    public ResultPanel(List<Movie> movies) {
        VerticalLayout verticalLayout = new VerticalLayout();

        resultPanelLabel = new Label("Result");
        textField = new TextField();
        textField.setSizeFull();

        grid = new Grid();
        grid.setContainerDataSource(new BeanItemContainer<>(Movie.class, movies));
        grid.setColumns("title", "rating", "description");

        grid.setSizeFull();

        verticalLayout.addComponent(resultPanelLabel);
        verticalLayout.addComponent(grid);
        verticalLayout.addComponent(textField);

        setContent(verticalLayout);
    }
}
